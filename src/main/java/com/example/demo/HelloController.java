package com.example.demo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.channels.Pipe;

public class HelloController {
    @FXML
    private Label welcomeText;
    @FXML
    private Circle myCircle;
    private double x;
    private double y;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void up (ActionEvent e){
        System.out.println("UP");
        myCircle.setCenterY(y-=5);
    }


    public void down (ActionEvent e){
        System.out.println("down");
        myCircle.setCenterY(y+=5);
    }


    public void left (ActionEvent e){
        System.out.println("left");
        myCircle.setCenterX(x-=5);
    }


    public void right (ActionEvent e){
        System.out.println("right");
        myCircle.setCenterX(x+=5);
    }



    public void switchToScene1(ActionEvent event) throws IOException{
        root = FXMLLoader.load(getClass().getResource("neww.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }



    public void switchToScene2(ActionEvent event) throws IOException{
        root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }




    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }
}